// Copyright (c) 2015, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library converters.base;

import 'dart:convert';

convertTo(Type type, String value) => value == null ||
    (type == String || type == dynamic) ? value : _codec(type).decode(value);

String convertFrom(Type type, dynamic value) =>
    value == null ? null : _codec(type).encode(value);

typedef _To(String s);

typedef String _From(v);

class SimpleTypeCodec<T> extends Codec<T, String> {
  @override
  final Converter<String, T> decoder;

  @override
  final Converter<T, String> encoder;

  SimpleTypeCodec(T to(String s), String from(T t))
      : this.decoder = new _ToConverter(to),
        this.encoder = new _FromConverter(from);
}

class _ToConverter<T> extends Converter<String, T> {
  final _To to;

  const _ToConverter(T to(String s)) : this.to = to;

  @override
  T convert(String input) => to(input);
}

class _FromConverter<T> extends Converter<T, String> {
  final _From from;

  const _FromConverter(String from(T t)) : this.from = from;

  @override
  String convert(T input) => from(input);
}

final SimpleTypeCodec<num> numCodec =
    new SimpleTypeCodec((s) => num.parse(s), (n) => n.toString());

final SimpleTypeCodec<bool> boolCodec =
    new SimpleTypeCodec((s) => s == "true" ? true : false, (b) => b.toString());

final SimpleTypeCodec<DateTime> dateTimeCodec =
    new SimpleTypeCodec((s) => DateTime.parse(s), (d) => d.toString());

final SimpleTypeCodec<Uri> uriCodec =
    new SimpleTypeCodec((s) => Uri.parse(s), (u) => u.toString());

final Map<Type, Codec<dynamic, String>> _stringCodecs = {
  num: numCodec,
  int: numCodec, // TODO: avoid by checking 'is'
  double: numCodec,
  bool: boolCodec,
  DateTime: dateTimeCodec,
  Uri: uriCodec,
  Map: JSON,
  List: JSON
};

_codec(Type type) {
  final codec = _stringCodecs[type];
  if (codec == null) {
    throw new ArgumentError('no codec for type $type');
  }
  return codec;
}
