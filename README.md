# converters

Simple type converters

## Usage

A simple usage example:

    import 'package:converters/converters.dart';

    main() {
      var myInt = convertTo(int, '10');
      var myString = convertFrom(int, 10);
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme
